﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace GeoCitizen.UI.AppPages
{
    public class PageStart
    {
        [FindsBy(How = How.XPath, Using = "//a//div[@class='md-button-content']")]
        public IWebElement BtnLogin { get; set; }
    }
}
 