﻿using GeoCitizen.UI.Wrapper;
using OpenQA.Selenium.Support.PageObjects;

namespace GeoCitizen.UI.AppPages
{
    public static class Pages
    {
        private static T GetPage<T>() where T : new()
        {
            var page = new T();
            PageFactory.InitElements(BrowserFactory.Driver, page);
            BrowserFactory.WaitForElement(10000);
            return page;
        }

        public static PageStart PageStart => GetPage<PageStart>();

        public static PageLogin PageLogin => GetPage<PageLogin>();

        public static PageGoogleAuth PageGoogleAuth => GetPage<PageGoogleAuth>();

        public static PageMap PageMap => GetPage<PageMap>();

        public static PageIssue PageIssue => GetPage<PageIssue>();
    }
}
