﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace GeoCitizen.UI.AppPages
{
    public class PageLogin
    {
        [FindsBy(How = How.XPath, Using = "//button[@class='btn btn-block btn-social btn-google']")]
        public IWebElement BtnSignWithGoogle { get; set; }
    }
}
