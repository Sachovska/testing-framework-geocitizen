﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace GeoCitizen.UI.AppPages
{
    public class PageGoogleAuth
    {
        [FindsBy(How = How.Id, Using = "identifierId")]
        public IWebElement InputEmail { get; set; }

        [FindsBy(How = How.Id, Using = "identifierNext")]
        public IWebElement BtnNext { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@class='whsOnd zHQkBf' and @type='password']")]
        public IWebElement InputPassword { get; set; }

        [FindsBy(How = How.Id, Using = "passwordNext")]
        public IWebElement BtnSignIn { get; set; }
    }
}
