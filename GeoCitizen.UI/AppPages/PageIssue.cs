﻿using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace GeoCitizen.UI.AppPages
{

    public class PageIssue
    {
        [FindsBy(How = How.Id, Using = "title")]
        public IWebElement InputTitle { get; set; }

        [FindsBy(How = How.ClassName, Using = "md-textarea")]
        public IWebElement InputDescription { get; set; }

        [FindsBy(How = How.CssSelector, Using = "i.md-icon:nth-child(2) > svg:nth-child(1)")]
        public IWebElement ExpandType { get; set; }

        [FindsBy(How = How.ClassName, Using = "md-list-item-text")]
        public IList<IWebElement> Types { get; set; }

        [FindsBy(How = How.ClassName, Using = "md-input")]
        public IWebElement BtnUploadImage { get; set; }

        [FindsBy(How = How.Id, Using = "uploadImage")]
        public IWebElement InputUploadImage { get; set; }

        [FindsBy(How = How.Id, Using = "save")]
        public IWebElement BtnCreate { get; set; }
        
        [FindsBy(How = How.ClassName, Using = "md-error")]
        public IList<IWebElement> ErrorMessages { get; set; }

        /// <summary>
        /// Create new issue
        /// </summary>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="type"></param>
        /// <param name="imagePath"></param>
        public void CreateIssue(string title, string description, string type, string imagePath)
        {
            InputTitle.SendKeys(title);
            InputDescription.SendKeys(description);
            if (!string.IsNullOrEmpty(type))
            {
                ExpandType.Click();
                SelectTypeByText(type);
            }
            if (!string.IsNullOrEmpty(imagePath))
            {
                InputUploadImage.SendKeys(imagePath);
            }
            BtnCreate.Click();
        }

        /// <summary>
        /// Select item by text
        /// </summary>
        /// <param name="value"></param>
        public void SelectTypeByText(string value)
        {
            ;
            foreach (var e in Types)
            {
                if (e.Text == value)
                {
                    e.Click();
                }
            }
        }
    }
}

