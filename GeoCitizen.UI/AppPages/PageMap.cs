﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace GeoCitizen.UI.AppPages
{
    public class PageMap
    {
        [FindsBy(How = How.Id, Using = "pac-input")]
        public IWebElement InputSearch { get; set; }

        [FindsBy(How = How.ClassName, Using = "pac-item")]
        public IList<IWebElement> DdlSearch { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#root")]
        public IWebElement MapArea { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='md-snackbar-content']//span")]
        public IWebElement WarningMessage { get; set; }

        /// <summary>
        /// Search location on the map
        /// </summary>
        /// <param name="location">Address or enstablishment on the map</param>
        public void SearchLocation(string location)
        {
            InputSearch.Click();
            InputSearch.SendKeys(location);
            DdlSearch.First().Click();
            MapArea.Click();
        }
    }
}

