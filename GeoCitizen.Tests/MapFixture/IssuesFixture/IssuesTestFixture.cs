﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using GeoCitizen.Common;
using GeoCitizen.UI.AppPages;
using GeoCitizen.UI.Wrapper;
using NUnit.Framework;

namespace GeoCitizen.Tests.MapFixture.IssuesFixture
{
    [TestFixture]
    class IssuesTestFixture : GeoCitizenBaseFixture
    {

        [Test]
        public void CreateIssueAsUnLoggedUser()
        {
            const string location = "Завод Кварц";
            const string warningMessage = "Please, log in to create an issue";

            // Login using google service
            BrowserFactory.InitBrowser(Settings.Browser);
            BrowserFactory.LoadApplication(Settings.Host);

            // Try to create issue > Observe warning messages
            Pages.PageMap.SearchLocation(location);
            string actualWarningMessage = Pages.PageMap.WarningMessage.Text;
            Assert.AreEqual(warningMessage, actualWarningMessage);

            // Log out
            BrowserFactory.CloseAllDrivers();
        }

        [Test]
        public void CreateEmptyIssue()
        {
            const string location = "Завод Кварц";
            var warningMessageOfFields = new List<string>
            {
                "Title is required",
                "Description is required",
                "Please, choose the type",
                "Please, attach an image"
            };

            // Login using google service
            BrowserFactory.InitBrowser(Settings.Browser);
            BrowserFactory.LoadApplication(Settings.Host);
            Pages.PageStart.BtnLogin.Click();
            Pages.PageLogin.BtnSignWithGoogle.Click();
            Pages.PageGoogleAuth.InputEmail.SendKeys(Settings.UserName);
            Pages.PageGoogleAuth.BtnNext.Click();
            Pages.PageGoogleAuth.InputPassword.SendKeys(Settings.Password);
            Pages.PageGoogleAuth.BtnSignIn.Click();

            // Create empty issue > Observe warning messages
            Pages.PageMap.SearchLocation(location);
            Pages.PageIssue.CreateIssue(string.Empty, string.Empty, string.Empty, string.Empty);
            var actualWarningMessageOfFields = Pages.PageIssue.ErrorMessages.Select(t=>t.Text).ToList();
            CollectionAssert.AreEqual(warningMessageOfFields, actualWarningMessageOfFields);

            // Log out
            BrowserFactory.CloseAllDrivers();
        }
        
        [Test]
        public void CreateNewIssue()
        {
            const string location = "Монарх";
            const string name = "New issue";
            const string description = "Here description of the issue";
            const string type = "Problem";
            string image = AppDomain.CurrentDomain.BaseDirectory + "\\" + "smile.png";

            // Login using google service
            BrowserFactory.InitBrowser(Settings.Browser);
            BrowserFactory.LoadApplication(Settings.Host);
            Pages.PageStart.BtnLogin.Click();
            Pages.PageLogin.BtnSignWithGoogle.Click();
            Pages.PageGoogleAuth.InputEmail.SendKeys(Settings.UserName);
            Pages.PageGoogleAuth.BtnNext.Click();
            Pages.PageGoogleAuth.InputPassword.SendKeys(Settings.Password);
            Pages.PageGoogleAuth.BtnSignIn.Click();

            // Create empty issue 
            Pages.PageMap.SearchLocation(location);
            Pages.PageIssue.CreateIssue(name, description, type, image);

            // Log out
            BrowserFactory.CloseAllDrivers();
        }
    }
}
