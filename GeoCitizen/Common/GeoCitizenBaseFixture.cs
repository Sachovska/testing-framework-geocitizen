﻿using GeoCitizen.DataClasses;

namespace GeoCitizen.Common
{
    public class GeoCitizenBaseFixture
    {
        protected GeoCitizenSettings Settings { get; } = new GeoCitizenSettings();
    }
}
