﻿namespace GeoCitizen.DataClasses
{
    public class GeoCitizenSettings
    {
        public string Browser { get; } = "Chrome";
        public string Host { get; } = "https://geocitizen.herokuapp.com";
        public string UserName { get; } = "sachovskavita@gmail.com";
        public string Password { get; } = "";
    }
}
